# KotlinCamp
Repositorio con ejercicios prácticos realizados en Kotlin, conceptos teoricos sobre Kotlin ...
[![Kotlin](https://img.shields.io/badge/Code-Kotlin-blueviolet)](https://kotlinlang.org/)
[![LISENCE](https://img.shields.io/badge/Lisence-MIT-green)]()
![GitHub](https://img.shields.io/github/last-commit/idanirf/KotlinCamp)

![imagen](./img/kotlin.png)

## Temas:
### 1. Kotlin - Introducción
### 2. Fundamentos del Lenguaje
### 3. Definción de Funciones
### 4. Trabajando con Clases y Objetos
### 5. Uso de Funciones Avanzadas y Programación Funcional
### 6. Uso de Clase de Casos Especiales
### 7. Explornando Colecciones
### 8. Comprendiendo Jerarquías de clase
### 9. Genéricos
### 10. Interoperabilidad de Java
### 11. Testing en Kotlin
<br>


## Contacto
* **Contacto:** daniel.rodriguezfernandez@alumno.iesluisvives.org
* **Twiter:** [@idanirf](https://twitter.com/idanirf)
* **Linkedin:** [Visita mi perfil](https://www.linkedin.com/in/danielrodriguezfernandez03002/)

[![Twitter](https://img.shields.io/twitter/follow/idanirf?style=social)](https://twitter.com/idanirf)
[![GitHub](https://img.shields.io/github/followers/idanirf?style=social)](https://github.com/idanirf)


